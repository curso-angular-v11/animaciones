import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('cambiaColor', [
      state('rojo', style({
        'background-color': 'red'
      })),
      state('amarillo', style({
        'background-color': 'yellow',
        'transform': 'scale(2)'
        // transform: con la propiedad transform podemos modificar la escala, la rotacion, podemos moverlo, etc.
      })),
      state('verde', style({
        'background-color': 'green'
      })),
      // transition('* => *', animate(500)): Si queremos de cualquier estado a otro estado cualquiera
      transition('rojo => amarillo', animate(500)),
      transition('amarillo => verde', animate(1000)),
      transition('verde => rojo', animate('.5s ease-in')),
      // en la ultima regla establezco q la animacion se de en 0.5 segundo y sea ease-in
      // ease-in: La animacion empieza suave al principio
      // ease-out: La animacion termina suave
      // ease-is: Empieza suave y termina suave
      // transition('void => *', [
      //   style({'transform': 'translateX(-100%)'}), //con la propiedad translate podemos enviar el objeto fuera de la pagina(q se oculte)
      //   animate(1000)
      // ])
      transition('void => *', [
        animate(500, keyframes([
          style({ opacity: 0, transform: 'translateX(-100%)', offset: 0}),
          style({ opacity: 1, transform: 'translateX(200px)', offset: 0.7}),
          style({ opacity: 1, transform: 'translateX(0)', offset: 1}),
        ]))
      ])
      // El estado void es el estado primigenio (estado en el que se encuentra antes de arrancar) del elemento o componente con el que vamos a interactuar.
      // Aparte del metodo animate podemos colocar un array con diferentes acciones
      // que pasaran durante la transicion

    ])
  ]
})
export class AppComponent {
  
color: string;

constructor(){
  this.color = 'verde'
}

ngOnInit() {
  setInterval(() => {
    if (this.color === 'rojo') {
      this.color = 'amarillo';
    } else if (this.color === "amarillo") {
      this.color = 'verde';
    } else if (this.color === 'verde') {
      this.color = 'rojo'
    }
  }, 2000)
}
}
